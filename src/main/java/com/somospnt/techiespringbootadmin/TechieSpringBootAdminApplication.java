package com.somospnt.techiespringbootadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@Controller
public class TechieSpringBootAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechieSpringBootAdminApplication.class, args);
    }
    
    @GetMapping("/")
    public String home() {
        return "home";
    }
    
}
